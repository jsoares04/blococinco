import java.util.Arrays;


public class ArrayBidimensionalExTres {
    //Attributes
    private Array[] arrayBidimensionalEx3;

    //Constructor

    public ArrayBidimensionalExTres() {

    }

    public ArrayBidimensionalExTres(Array[] rowsInput) {
        this.arrayBidimensionalEx3 = Arrays.copyOf(rowsInput, rowsInput.length);
    }

    /**
     * Equals override.
     *
     * @param other final Object other
     * @return true if objects are equals.
     */
    @Override
    public boolean equals(final Object other) {
        if (this == other) {
            return true;
        }
        if (!(other instanceof ArrayBidimensionalExTres)) {
            return false;
        }
        ArrayBidimensionalExTres otherMatrix = (ArrayBidimensionalExTres) other;
        return Arrays.deepEquals(arrayBidimensionalEx3, otherMatrix.arrayBidimensionalEx3);
    }

    /**
     * hashCode Override.
     *
     * @return hashCode override
     */
    @Override
    public int hashCode() {
        return Arrays.hashCode(arrayBidimensionalEx3);
    }


    //Operations

    /**
     * adda a new value on a indicated row
     *
     * @param value
     * @param row
     */
    private void addNewValueInARow(int value, int row) {
        Array modifiedRow = this.arrayBidimensionalEx3[row];
        modifiedRow.addNewArrayElement(value);
        this.arrayBidimensionalEx3[row] = modifiedRow;
    }

    public void callAddValue(int value, int row) {
        this.addNewValueInARow(value, row);
    }

    /**
     * return the number of rows of the matrix.
     *
     * @return
     */
    private int getLength() {
        return this.arrayBidimensionalEx3.length;
    }

    /**
     * remove the firt value to apear on th matrix, checking the lines first.
     *
     * @param value
     */

    private void removeFirstElement(int value) {
        int i;
        for (i = 0; i < this.getLength(); i++) {
            Array modifiedRow = this.arrayBidimensionalEx3[i];
            modifiedRow.removeFirstElementOfIndicatedValue(value);
            if (!modifiedRow.equals(this.arrayBidimensionalEx3[i])) {
                this.arrayBidimensionalEx3[i] = modifiedRow;
                i = this.getLength();
            }
        }
    }

    public void callToRemoveFirstElement(int value) {
        this.removeFirstElement(value);
    }

    /**
     * check of the matrix is empty
     *
     * @return
     */
    public boolean checkMatrixEmpty() {
        if (this.arrayBidimensionalEx3.length == 0) {
            return true;
        } else return false;
    }

    /**
     * return the biggest value of the array
     *
     * @return
     */

    public int returnBiggestValue() {
        int i = 0, bigger = 0;
        for (i = 0; i < this.getLength(); i++) {
            Array tempRow = this.arrayBidimensionalEx3[i];
            int biggerFromRow = tempRow.returnGreaterValue();
            if (biggerFromRow > bigger) {
                bigger = biggerFromRow;
            }
        }
        return bigger;
    }

    /**
     * return the lowest value of the array
     *
     * @return
     */

    public int returnLowestValue() {
        int i, lowest = 20000000;
        for (i = 0; i < this.getLength(); i++) {
            Array tempRow = this.arrayBidimensionalEx3[i];
            int lowestFromRow = tempRow.returnSmallerValue();
            if (lowestFromRow < lowest) {
                lowest = lowestFromRow;
            }
        }
        return lowest;
    }

    /**
     * return average
     * @return
     */
    public double returnAverage() {
        int i;
        double sum = 0;
        for (i = 0; i < this.getLength(); i++) {
            Array tempRow = this.arrayBidimensionalEx3[i];
            sum += tempRow.returnMedium();
        }
        return sum/this.getLength();
    }

    public int[] returnSumOfRows(){
        int i;
        int[] sum = new int[this.getLength()];
        for ( i = 0; i < this.getLength(); i++) {
            Array tempRow = this.arrayBidimensionalEx3[i];
            sum[i] = tempRow.sumOfVector();
        }
        return sum;
    }


}

