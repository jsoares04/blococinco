import java.util.Arrays;


public class ArrayBidimensional {
    //Attributes
    private int[][] arrayBidimensional;

    //Constructor

    public ArrayBidimensional() {
        this.arrayBidimensional = new int[0][0];
    }

    public ArrayBidimensional(int[][] array) {
        this.arrayBidimensional = array;
    }

    //Operations

    /**
     * add a value to a bidimensional array indicating the value and the row
     *
     * @param value
     * @param row
     * @return
     */

    private int[][] editMatrix(int value, int row) {
        int[][] tempMatrix = new int[this.arrayBidimensional.length][this.arrayBidimensional[0].length];
        System.arraycopy(this.arrayBidimensional, 0, tempMatrix, 0, this.arrayBidimensional.length);
        tempMatrix[row] = Arrays.copyOf(this.arrayBidimensional[row], this.arrayBidimensional[row].length + 1);
        tempMatrix[row][this.arrayBidimensional[row].length] = value;
        return tempMatrix;
    }

    /**
     * call the private function to add an element to a bidimensional array
     *
     * @param value
     * @param row
     * @return
     */

    public ArrayBidimensional addElement(int value, int row) {
        this.arrayBidimensional = editMatrix(value, row);
        return this;
    }

    /**
     * remove the first element equal to an indicated value.
     *
     * @param value
     * @return
     */
    public int[][] removeFirstValueOfTheArray(int value) {
        int[][] tempMatrix = new int[this.arrayBidimensional.length][this.arrayBidimensional[0].length];
        System.arraycopy(this.arrayBidimensional, 0, tempMatrix, 0, this.arrayBidimensional.length);
        int i, j, row = 0, column = 0, columnMax;
        for (i = 0; i < tempMatrix.length; i++) {
            columnMax = tempMatrix[i].length;
            for (j = 0; j < columnMax; j++) {
                if (tempMatrix[i][j] == value) {
                    row = i;
                    column = j;
                    j = tempMatrix[i].length;
                    i = tempMatrix.length;
                }
            }
        }
        int[] temp = this.arrayBidimensional[row];
        for (i = column; i < this.arrayBidimensional[row].length - 1; i++) {
            temp[i] = this.arrayBidimensional[row][i + 1];
        }
        temp = Arrays.copyOf(temp, this.arrayBidimensional[row].length - 1);
        tempMatrix[row] = temp;
        return tempMatrix;
    }

    /**
     * call the private function to remove an element to a bidimensional array.
     *
     * @param value
     * @return
     */
    public ArrayBidimensional removeElement(int value) {
        this.arrayBidimensional = removeFirstValueOfTheArray(value);
        return this;
    }

    /**
     * ckeck matrix dimension.
     *
     * @return true if empty, false if not empty.
     */

    public Boolean checkMatrixDimension() {
        if (this.arrayBidimensional.length == 0) {
            return true;
        } else return false;
    }

    /**
     * check number of rows of a bidimensional Array.
     *
     * @return
     */
    private int checkNumberOfRows() {
        return this.arrayBidimensional.length;
    }

    private int checkNumberOfColumnsOfIndicatedRow(int row) {
        return this.arrayBidimensional[row].length;
    }

    /**
     * check Bigger Value of an matrix.
     *
     * @return
     */
    private int checkBiggerValue() {
        int i, j, checker = this.arrayBidimensional[0][0];
        for (i = 0; i < this.checkNumberOfRows(); i++) {
            for (j = 0; j < this.checkNumberOfColumnsOfIndicatedRow(i); j++) {
                if (this.arrayBidimensional[i][j] > checker) {
                    checker = this.arrayBidimensional[i][j];
                }
            }
        }
        return checker;
    }

    /**
     * return bigger value of an matrix.
     *
     * @return
     */
    public int returnBiggerValue() {
        return checkBiggerValue();
    }

    /**
     * check the lowest value of a Matrix.
     *
     * @return
     */

    private int checkLowestValue() {
        int i, j, checker = this.arrayBidimensional[0][0];
        for (i = 0; i < this.checkNumberOfRows(); i++) {
            for (j = 0; j < this.checkNumberOfColumnsOfIndicatedRow(i); j++) {
                if (this.arrayBidimensional[i][j] < checker) {
                    checker = this.arrayBidimensional[i][j];
                }
            }
        }
        return checker;
    }

    /**
     * check the lowest value of an array.
     *
     * @return
     */
    public int returnLowestValue() {
        return checkLowestValue();
    }

    /**
     * calculate the average of all elements of th matrix.
     *
     * @return
     */
    private double calculateAverage() {
        int sum = 0, counter = 0, i, j;
        for (i = 0; i < checkNumberOfRows(); i++) {
            for (j = 0; j < checkNumberOfColumnsOfIndicatedRow(i); j++) {
                sum = sum + arrayBidimensional[i][j];
                counter++;
            }
        }
        return Math.round(((double) sum / counter) * 100.00) / 100.00;
    }

    public double returnAverage() {
        return calculateAverage();
    }

    /**
     * check if a matrix is square.
     *
     * @return
     */
    public boolean checkSquareArray() {
        int i;
        boolean check = true;
        for (i = 0; i < this.checkNumberOfRows(); i++) {
            if (this.checkNumberOfRows() != this.checkNumberOfColumnsOfIndicatedRow(i)) {
                check = false;
            }
        }
        return check;
    }

    public boolean checkSymmetricArray() {
        int i, j;
        boolean check = true;
        if (checkSquareArray()) {
            for (i = 0; i < this.checkNumberOfRows(); i++) {
                for (j = 0; j < this.checkNumberOfColumnsOfIndicatedRow(i); j++) {
                    if (this.arrayBidimensional[i][j] != this.arrayBidimensional[j][i]) {
                        check = false;

                    }
                }
            }
            return check;
        } else return false;
    }

    private int checkNumberOfNotNullValuesOnDiagonal() {
        int i, countNotNull=0;
        if (checkSquareArray()) {
            for (i = 0; i < this.checkNumberOfRows(); i++) {
                if (this.arrayBidimensional[i][i] != 0) {
                    countNotNull++;
                }
            }
        return countNotNull;
        } else return -1;

    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ArrayBidimensional that = (ArrayBidimensional) o;
        return Arrays.deepEquals(arrayBidimensional, that.arrayBidimensional);
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(arrayBidimensional);
    }
}
