import java.util.Arrays;

public class Array {
    //Attributes
    private int[] array;

    //Constructor

    /**
     * constructor array without values 1a)
     */
    public Array() {
        this.array = new int[0];
    }

    /**
     * constructor to initialize one vector with some values 1b)
     *
     * @param array
     */
    public Array(int[] array) {
        this.array = Arrays.copyOf(array, array.length);
    }

    //Operations

    /**
     * add a new element in the last postition of an array
     *
     * @param newValue an integer with the new value
     * @return return the array with the new value
     */
    public Array addNewArrayElement(int newValue) {
        int newPosition = this.array.length;
        this.array = Arrays.copyOf(this.array, newPosition + 1);
        this.array[newPosition] = newValue;
        return this;
    }

    /**
     * remove First element of indicated Value
     *
     * @param value
     * @return
     */

    public void removeFirstElementOfIndicatedValue(int value) {
        int i = 0;
        boolean j = false;
        int[] arrayOne = new int[returnLengthOfArray() - 1];
        for (i = 0; i < returnLengthOfArray() - 1; i++) {
            if (this.array[i] == value || j) {
                arrayOne[i] = this.array[i + 1];
                j = true;
            } else arrayOne[i] = array[i];
        }
        this.array = arrayOne;
    }

    /**
     * remove element from position
     *
     * @param position
     * @return
     */

    public void removeElementFromIndicatedPosition(int position) {
        int i = 0;
        int[] arrayOne = new int[returnLengthOfArray() - 1];
        if (returnLengthOfArray() > position) {
            for (i = 0; i < (returnLengthOfArray() - 1); i++) {
                if (i >= position) {
                    arrayOne[i] = this.array[i + 1];
                } else {
                    arrayOne[i] = this.array[i];
                }
            }
        } else throw new IllegalArgumentException("Position doesnt exist");
        this.array = arrayOne;
    }

    /**
     * return a value from a indicated postition
     *
     * @param position
     * @return
     */
    public int returnElementFromIndicatedPosition(int position) {
        if (returnLengthOfArray() > position) {
            return this.array[position];
        } else throw new IllegalArgumentException("Position doesnt exist");
    }

    /**
     * return the lenght of an array.
     *
     * @return
     */

    public int returnLengthOfArray() {
        return this.array.length;
    }

    /**
     * return the bigger value of an array
     *
     * @return
     */

    public int returnGreaterValue() {
        int i;
        int bigger = 0;
        for (i = 0; i < returnLengthOfArray(); i++) {
            if (array[i] > bigger) {
                bigger = array[i];
            }
        }
        return bigger;
    }

    /**
     * return the smaller value of an array
     *
     * @return
     */

    public int returnSmallerValue() {
        int i;
        int smaller = 500000;
        for (i = 0; i < returnLengthOfArray(); i++) {
            if (array[i] < smaller) {
                smaller = array[i];
            }
        }
        return smaller;
    }

    /**
     * return the medium of an array
     *
     * @return
     */

    public double returnMedium() {
        int i;
        int sum = 0;
        for (i = 0; i < returnLengthOfArray(); i++) {
            sum = sum + array[i];
        }
        return (double) sum / returnLengthOfArray();
    }

    /**
     * return the medium of even values of an array
     *
     * @return
     */

    public double returnEvenValuesMedium() {
        int i, evenCounter = 0;
        int sum = 0;
        for (i = 0; i < returnLengthOfArray(); i++) {
            if (array[i] % 2 == 0) {
                sum += array[i];
                evenCounter++;
            }
        }
        return (double) sum / evenCounter;
    }


    /**
     * return the medium of even values of an array.
     *
     * @return
     */
    public double returnOddValuesMedium() {
        int i, oddCounter = 0;
        int sum = 0;
        for (i = 0; i < returnLengthOfArray(); i++) {
            if (array[i] % 2 != 0) {
                sum += array[i];
                oddCounter++;
            }
        }
        return (double) sum / oddCounter;
    }

    /**
     * sort an array
     *
     * @return
     */
    public Array sortAscendingArray() {
        Arrays.sort(this.array);
        return this;
    }

    public boolean checkArrayEmpty() {
        if (returnLengthOfArray() == 0) {
            return true;
        } else return false;
    }

    /**
     * check one element array
     *
     * @return
     */
    public boolean checkOneElementArray() {
        if (returnLengthOfArray() == 1) {
            return true;
        } else return false;
    }

    public boolean checkIfArrayOnlyEvenNumbers() {
        int i;
        boolean check = true;
        for (i = 0; i < returnLengthOfArray(); i++) {
            if (array[i] % 2 != 0) {
                check = false;
                i = returnLengthOfArray();
            }
        }
        return check;

    }


    /**
     * check if the array only have Odd Numbers
     *
     * @return
     */
    public boolean checkIfArrayOnlyOddNumbers() {
        int i;
        boolean check = true;
        for (i = 0; i < returnLengthOfArray(); i++) {
            if (array[i] % 2 == 0) {
                check = false;
                i = returnLengthOfArray();
            }
        }
        return check;
    }

    /**
     * check duplicated values on an array.
     *
     * @return
     */

    public boolean checkDuplicatedNumbers() {
        int i, j;
        boolean check = false;
        for (i = 0; i < returnLengthOfArray(); i++) {
            for (j = 0; j < returnLengthOfArray(); j++) {
                if (array[i] == array[j] && i != j) {
                    check = true;
                    j = returnLengthOfArray();
                    i = j;
                }
            }
        }
        return check;
    }

    public int[] checkValuesBiggerThanMedium() {
        int i, j = 0;
        int[] biggerThanMedium = new int[returnLengthOfArray()];
        for (i = 0; i < returnLengthOfArray(); i++) {
            if (this.array[i] > this.returnMedium()) {
                biggerThanMedium[j] = this.array[i];
                j++;
            }
        }
        return Arrays.copyOf(biggerThanMedium, j);
    }

    public int sumOfVector() {
        int sum = 0;
        for (int i = 0; i < this.returnLengthOfArray(); i++) {
            sum += this.array[i];
        }
        return sum;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Array array1 = (Array) o;
        return Arrays.equals(array, array1.array);
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(array);
    }
}


