import org.junit.Assert;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


class ArrayBidimensionalTest {

    @Test
    public void addElementToMatrix() {
        int[][] input = {{1, 2, 3}, {1, 2, 3}, {1, 2, 3}};
        ArrayBidimensional test = new ArrayBidimensional(input);
        ArrayBidimensional expected = new ArrayBidimensional(new int[][]{{1, 2, 3}, {1, 2, 3, 4}, {1, 2, 3}});
        ArrayBidimensional result = test.addElement(4, 1);
        Assert.assertEquals(expected, result);
    }

    @Test
    public void removeFirstElementOfMatrix() {
        int[][] input = {{1, 2}, {1, 2,3}, {1, 2, 3, 4}};
        ArrayBidimensional test = new ArrayBidimensional(input);
        ArrayBidimensional expected = new ArrayBidimensional(new int[][]{{1, 2}, {1, 2}, {1, 2, 3, 4}});
        test.removeElement(3);
        Assert.assertEquals(expected, test);
    }

    @Test
    public void checkMatrixDimension() {
        int[][] input = {{1, 2}, {1, 2, 3}, {1, 2, 3, 4}};
        ArrayBidimensional test = new ArrayBidimensional(input);
        boolean expected = false;
        boolean result = test.checkMatrixDimension();
        Assert.assertEquals(expected, result);
    }

    @Test
    public void checkMatrixDimensionEmpty() {
        int[][] input = {};
        ArrayBidimensional test = new ArrayBidimensional(input);
        boolean expected = true;
        boolean result = test.checkMatrixDimension();
        Assert.assertEquals(expected, result);
    }

    @Test
    public void checkBiggerValue() {
        int[][] input = {{1, 2}, {1, 2, 8}, {1, 2, 3, 4}};
        ArrayBidimensional test = new ArrayBidimensional(input);
        int expected = 8;
        int result = test.returnBiggerValue();
        Assert.assertEquals(expected, result);
    }

    @Test
    public void checkLowestValue() {
        int[][] input = {{1, 2}, {1, 2, 8}, {1, 2, 3, 4}};
        ArrayBidimensional test = new ArrayBidimensional(input);
        int expected = 1;
        int result = test.returnLowestValue();
        Assert.assertEquals(expected, result);
    }

    @Test
    public void checkAverage() {
        int[][] input = {{1, 2}, {1, 2, 8}, {1, 2, 3, 4}};
        ArrayBidimensional test = new ArrayBidimensional(input);
        double expected = 2.67;
        double result = test.returnAverage();
        Assertions.assertEquals(expected, result);
    }

    @Test
    public void CheckMatrixSquare() {
        int[][] input = {{1, 2}, {1, 2}};
        ArrayBidimensional test = new ArrayBidimensional(input);
        boolean expected = true;
        boolean result = test.checkSquareArray();
        Assert.assertEquals(expected, result);
    }
    @Test
    public void CheckMatrixSymmetric() {
        int[][] input = {{1, 2},
                         {2, 1}};
        ArrayBidimensional test = new ArrayBidimensional(input);
        boolean expected = true;
        boolean result = test.checkSymmetricArray();
        Assert.assertEquals(expected, result);
    }
}