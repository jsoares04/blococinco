import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;

class ArrayBidimensionalExTresTest {
    Array rowZero = new Array(new int[]{1, 2});
    Array rowOne = new Array(new int[]{1, 2});

    @Test
    void insertValueXToRowY() {
        Array[] original = new Array[]{rowZero, rowOne};
        ArrayBidimensionalExTres originalMatrix = new ArrayBidimensionalExTres(original);
        final int valueToInsert = 3;
        final int rowToInsert = 0;
        Array rowZeroExpected = new Array(new int[]{1, 2, 3});
        Array[] expected = new Array[]{rowZeroExpected,
                rowOne};
        ArrayBidimensionalExTres expectedMatrix = new ArrayBidimensionalExTres(expected);

        originalMatrix.callAddValue(valueToInsert
                , rowToInsert);

        assertEquals(expectedMatrix, originalMatrix);
    }

    @Test
    void removeValueXFirst() {
        Array[] original = new Array[]{rowZero, rowOne};
        ArrayBidimensionalExTres originalMatrix = new ArrayBidimensionalExTres(original);
        final int valueToRemove = 2;
        Array rowZeroExpected = new Array(new int[]{1});
        Array[] expected = new Array[]{rowZeroExpected,
                rowOne};
        ArrayBidimensionalExTres expectedMatrix = new ArrayBidimensionalExTres(expected);

        originalMatrix.callToRemoveFirstElement(valueToRemove);

        assertEquals(expectedMatrix, originalMatrix);
    }
    @Test
    public void checkMatrixDimension() {
        Array[] original = new Array[]{rowZero, rowOne};
        ArrayBidimensionalExTres originalMatrix = new ArrayBidimensionalExTres(original);
        boolean expected = false;
        boolean result =originalMatrix.checkMatrixEmpty();

        assertEquals(expected, result);
    }
    @Test
    void returnBiggest() {
        Array[] original = new Array[]{rowZero, rowOne};
        ArrayBidimensionalExTres originalMatrix = new ArrayBidimensionalExTres(original);
        int expected = 2;


        int result = originalMatrix.returnBiggestValue();

        assertEquals(expected, result);
    }
    @Test
    void returnLowest() {
        Array[] original = new Array[]{rowZero, rowOne};
        ArrayBidimensionalExTres originalMatrix = new ArrayBidimensionalExTres(original);
        int expected = 1;
        int result = originalMatrix.returnLowestValue();
        assertEquals(expected, result);
    }
    @Test
    void returnMedium() {
        Array[] original = new Array[]{rowZero, rowOne};
        ArrayBidimensionalExTres originalMatrix = new ArrayBidimensionalExTres(original);
        double expected = 1.5;
        double result = originalMatrix.returnAverage();
        assertEquals(expected, result);
    }
    @Test
    void returnArrayWithRowSums(){
        Array[] original = new Array[]{rowZero, rowOne};
        ArrayBidimensionalExTres originalMatrix = new ArrayBidimensionalExTres(original);
        int[] expected = new int[]{3,3};
        int[] result = originalMatrix.returnSumOfRows();
        assertArrayEquals(expected,result);
    }

}