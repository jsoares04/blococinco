import org.junit.Assert;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class ArrayTest {
    /**
     * test to add a new array element
     */
    @Test
    public void testAddNewArrayElement() {
        int[] input = {1, 2, 3};
        Array teste = new Array(input);
        Array expected = new Array(new int[]{1, 2, 3, 4});
        Array result = teste.addNewArrayElement(4);
        Assert.assertEquals(result, expected);
    }


    /**
     * test remove first element equal to a value.
     */
    @Test
    public void testeRemoveFirstElement() {
        int[] input = {1, 2, 3};
        Array test = new Array(input);
        Array expected = new Array(new int[]{1, 2});
         test.removeFirstElementOfIndicatedValue(3);
        Assert.assertEquals(expected, test);
    }

    /**
     * test remove a element from a middle indicated position.
     */
    @Test
    public void testRemoveElementFromPosition() {
        Array test = new Array(new int[]{1, 2, 3});
        Array expected = new Array(new int[]{1, 3});
       test.removeElementFromIndicatedPosition(1);
        Assert.assertEquals(expected, test);
    }

    /**
     * test remove a element from an end indicated position.
     */
    @Test
    public void testRemoveElementFromPositionLastPosition() {
        Array test = new Array(new int[]{1, 2, 3});
        Array expected = new Array(new int[]{1, 2});
        test.removeElementFromIndicatedPosition(2);
        Assert.assertEquals(expected, test);
    }

    /**
     * test returning the value from the indicated position
     */
    @Test
    public void testReturnElementFromPosition() {
        int[] input = {1, 2, 3};
        Array test = new Array(input);
        int expected = 2;
        int result = test.returnElementFromIndicatedPosition(1);
        Assert.assertEquals(expected, result);
    }

    @Test
    public void checkExceptionPostitionDoesntExist() {
        Array teste = new Array(new int[]{1, 2, 3});
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            teste.returnElementFromIndicatedPosition(3);
        });
    }

    @Test
    public void testArrayLength() {
        int[] input = {1, 2, 3};
        Array test = new Array(input);
        int expected = 3;
        int result = test.returnLengthOfArray();
        Assert.assertEquals(expected, result);
    }

    @Test
    public void testArrayBiggerValue() {
        int[] input = {1, 2, 3};
        Array test = new Array(input);
        int expected = 3;
        int result = test.returnGreaterValue();
        Assert.assertEquals(expected, result);
    }

    @Test
    public void testArraySmallerValue() {
        int[] input = {1, 2, 3};
        Array test = new Array(input);
        int expected = 1;
        int result = test.returnSmallerValue();
        Assert.assertEquals(expected, result);
    }

    @Test
    public void testArrayMedium() {
        int[] input = {1, 2, 3};
        Array test = new Array(input);
        int expected = 2;
        double result = test.returnMedium();
        Assert.assertEquals(expected, result, 0);
    }

    @Test
    public void testArrayEvenMedium() {
        int[] input = {1, 2, 3, 4};
        Array test = new Array(input);
        int expected = 3;
        double result = test.returnEvenValuesMedium();
        Assert.assertEquals(expected, result, 0);
    }

    @Test
    public void testArrayOddMedium() {
        int[] input = {1, 2, 3, 4};
        Array test = new Array(input);
        int expected = 2;
        double result = test.returnOddValuesMedium();
        Assert.assertEquals(expected, result, 0);
    }

    @Test
    public void testSortArray() {
        int[] input = {4, 3, 2, 1};
        Array test = new Array(input);
        Array expected = new Array(new int[]{1, 2, 3, 4});
        Array result = test.sortAscendingArray();
        Assert.assertEquals(expected, result);
    }

    @Test
    public void emptyArray() {
        int[] input = {};
        Array test = new Array(input);
        boolean expected = true;
        boolean result = test.checkArrayEmpty();
        Assert.assertEquals(expected, result);
    }

    @Test
    public void emptyTwoArray() {
        int[] input = {0, 0};
        Array test = new Array(input);
        boolean expected = false;
        boolean result = test.checkOneElementArray();
        Assert.assertEquals(expected, result);
    }

    @Test
    public void onlyEvenArray() {
        int[] input = {2, 4, 6, 8};
        Array test = new Array(input);
        boolean expected = true;
        boolean result = test.checkIfArrayOnlyEvenNumbers();
        Assert.assertEquals(expected, result);
    }

    @Test
    public void onlyOddArray() {
        int[] input = {1, 3, 5, 7};
        Array test = new Array(input);
        boolean expected = true;
        boolean result = test.checkIfArrayOnlyOddNumbers();
        Assert.assertEquals(expected, result);
    }

    @Test
    public void arrayWithoutDuplicates() {
        int[] input = {1, 3, 5, 7};
        Array test = new Array(input);
        boolean expected = false;
        boolean result = test.checkDuplicatedNumbers();
        Assert.assertEquals(expected, result);
    }

    @Test
    public void arrayWithDuplicates() {
        int[] input = {1, 3, 5, 7, 7};
        Array test = new Array(input);
        boolean expected = true;
        boolean result = test.checkDuplicatedNumbers();
        Assert.assertEquals(expected, result);
    }

    @Test
    public void arrayWithValuesBiggerThanMedium() {
        int[] input = {1, 3, 5, 7, 7};
        Array test = new Array(input);
        int[] expected = new int[]{5, 7, 7};
        int[] result = test.checkValuesBiggerThanMedium();
        Assert.assertArrayEquals(expected, result);
    }
    @Test
    public void sumOfVectorValues(){
        Array teste = new Array(new int[]{1,2,3,4});
        int expected = 10;
        int result = teste.sumOfVector();
        Assert.assertEquals(expected,result);
    }




}